# Telegram bot example app

## Acknowledgments
- https://medium.com/analytics-vidhya/building-a-simple-chatbot-in-python-using-nltk-7c8c8215ac6e
- https://medium.com/python4you/creating-telegram-bot-and-deploying-it-on-heroku-471de1d96554

## Run tests

    $ make test

## Deploy application and watch logs

    $ heroku container:login
    $ make deploy
    $ make tail
