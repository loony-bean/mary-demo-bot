# coding: utf-8

import nltk
import random
import string
import warnings

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

warnings.filterwarnings("ignore")
random.seed(1984)

content = open('chatbot.txt').read().lower()
sent_tokens = nltk.sent_tokenize(content)  # converts to list of sentences
word_tokens = nltk.word_tokenize(content)  # converts to list of words


def lemmatizer(tokens):
    """
    Lemmatizer.

    >>> lemmatizer(["users", "could", "write", "messages"])
    ['user', 'could', 'write', 'message']
    """
    lemmer = nltk.stem.WordNetLemmatizer()
    return [lemmer.lemmatize(token) for token in tokens]


def tokenizer(text):
    """
    Tokenizer.

    >>> tokenizer("some text, etc")
    ['some', 'text', 'etc']
    """
    text = text.lower()
    remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
    text = text.translate(remove_punct_dict)
    return lemmatizer(nltk.word_tokenize(text))


def greeting(sentence):
    """
    If user's input is a greeting, return a greeting response

    >>> greeting("hi there!")
    '*nods*'
    >>> greeting("bye")
    """
    inputs = [
        "hello",
        "hi",
        "greetings",
        "sup",
        "what's up",
        "hey"
    ]
    responses = [
        "hi",
        "hey",
        "*nods*",
        "hi there",
        "hello",
        "I am glad! You are talking to me"
    ]
    for word in sentence.split():
        if word.lower() in inputs:
            return random.choice(responses)


def intro():
    return "My name is Beemo. I will answer your queries about Chatbots."


# Generating response
def response(sentence):
    """
    Pick appropriate sentence from the data file.

    >>> response("ok tell me about chatbot APIs")
    'apis\\nthere are many apis available for building your own chatbots, such as aarc.'
    """
    sent_tokens.append(sentence)
    vectorizer = TfidfVectorizer(tokenizer=tokenizer, stop_words='english')
    tfidf = vectorizer.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx = vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    sent_tokens.remove(sentence)
    if (req_tfidf == 0):
        return "I am sorry! I don't understand you"
    else:
        return sent_tokens[idx]


if __name__ == "__main__":
    def writeln(text):
        print("> [Beemo] " + text)

    writeln(intro())

    while True:
        text = input().lower()

        if text == "bye":
            writeln("Bye! take care..")
            break

        if text == "thanks" or text == "thank you":
            writeln("You are welcome..")
            break

        if (greeting(text)):
            writeln(greeting(text))
        else:
            writeln(response(text))
