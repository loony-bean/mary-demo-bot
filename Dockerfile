FROM python:3.7

# Grab requirements.txt
ADD ./requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip install -qr /tmp/requirements.txt

# Add our code
ADD . /opt/demo-telegram-bot/
WORKDIR /opt/demo-telegram-bot

# Download corpora
RUN python -c "import nltk; nltk.download('punkt', download_dir='/usr/share/nltk_data')"
RUN python -c "import nltk; nltk.download('wordnet', download_dir='/usr/share/nltk_data')"

# Entry point
CMD python demo-telegram-bot.py
