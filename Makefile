APP=mary-demo-bot

check:
	pipenv run flake8 --max-line-length=120

test: clean check
	pipenv run python -m doctest -v *.py

deps:
	pipenv run pip freeze > requirements.txt

clean:
	rm -rf *.pyc
	rm -rf requirements.txt

deploy: test deps
	heroku container:push --app ${APP} web
	heroku container:release --app ${APP} web

tail:
	heroku logs --tail --app ${APP}
