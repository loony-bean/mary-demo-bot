# coding: utf-8

import logging
import os

import model

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from dotenv import load_dotenv
load_dotenv()

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()

TOKEN = os.getenv("TOKEN")

if os.getenv("MODE") == "prod":
    def run(updater):
        PORT = int(os.environ.get("PORT", "8443"))
        HEROKU_APP_NAME = os.environ.get("HEROKU_APP_NAME")
        updater.start_webhook(listen="0.0.0.0",
                              port=PORT,
                              url_path=TOKEN)
        updater.bot.set_webhook("https://{}.herokuapp.com/{}".format(HEROKU_APP_NAME, TOKEN))
else:
    def run(updater):
        updater.start_polling()
        updater.idle()


def start(bot, update):
    logger.info("User {} started bot".format(update.effective_user["id"]))
    text = model.intro()
    update.message.reply_text(text)


def echo(bot, update):
    text = update.message.text.lower()
    if (model.greeting(text)):
        response = model.greeting(text)
    else:
        response = model.response(text)

    update.message.reply_text(response)


if __name__ == '__main__':
    logger.info("Starting bot")
    updater = Updater(TOKEN)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(MessageHandler(Filters.text, echo))

    run(updater)
